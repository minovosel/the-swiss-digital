import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutComponent} from './layout/layout.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {UserListComponent} from './layout/user-list/user-list.component';
import {UserComponent} from './layout/user-list/user/user.component';
import {HttpClientModule} from '@angular/common/http';
import {MainRoutingModule} from './main-routing.module';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [HeaderComponent, FooterComponent, UserListComponent, UserComponent, LayoutComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    MainRoutingModule,
    NgbPaginationModule
  ],
  exports: [LayoutComponent]
})
export class MainModule { }
