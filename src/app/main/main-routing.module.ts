import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserListComponent} from './layout/user-list/user-list.component';
import {UserComponent} from './layout/user-list/user/user.component';

const mainRoutes: Routes = [
  {
    path: '',
    component: UserListComponent,
    pathMatch: 'full',
  },
  {
    path: 'user/:login',
    component: UserComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    [RouterModule.forChild(mainRoutes)]
  ],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
