import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserListService} from '../services/user-list.service';
import {UserModel} from '../models/user-list.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  username: string;
  user: UserModel;
  followers: UserModel[];
  headElements = ['', 'ID', 'Name', ''];

  page = 1;
  pageSize = 10;

  constructor(
    private route: ActivatedRoute,
    private userListService: UserListService
  ) {
  }

  ngOnInit() {
    this.route.params.subscribe(val => {
      this.username = val.login;

      // get User data after user username/login has been fetched
      this.userListService.getUserByLogin(this.username)
        .subscribe(
          (response: UserModel) => this.user = response
        );

      // get User followers
      this.userListService.getUserFollowersByLogin(this.username)
        .subscribe(
          (response: UserModel[]) => this.followers = response
        );
    });
  }

}
