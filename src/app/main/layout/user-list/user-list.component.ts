import {Component, OnInit} from '@angular/core';
import {UserListModel, UserModel} from './models/user-list.model';
import {UserListService} from './services/user-list.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  listOfUser: UserModel[] = [];
  headElements = ['', 'ID', 'Name', 'Type', 'Score', ''];

  constructor(
    private userListService: UserListService
  ) {
  }

  ngOnInit() {
    this.getUserList();
  }

  getUserList(username?: string) {
    this.userListService.getUsers(username)
      .subscribe(
        (response: UserListModel) => {
          const list = response.items;
          this.listOfUser = Object.entries(list).slice(0, 10)
            .map(entry => entry[1]);
        });
  }

  searchByUsername(username: string) {
    this.getUserList(username);
    // This list could have been updated on explicit click, but I've choose keyup event to have filter effect
  }

}
