import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserListModel, UserModel} from '../models/user-list.model';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserListService {

  constructor(
    protected httpClient: HttpClient
  ) {
  }

  getUsers(username?: string): Observable<UserListModel> {
    if (!username || username === '') {
      username = 'test';
    }
    return this.httpClient.get<UserListModel>(`https://api.github.com/search/users?q=${username}`);
  }

  getUserByLogin(username: string): Observable<UserModel> {
    return this.httpClient.get<UserModel>(`https://api.github.com/users/${username}`);
  }

  getUserFollowersByLogin(username: string): Observable<UserModel[]> {
    return this.httpClient.get<UserModel[]>(`https://api.github.com/users/${username}/followers`);
  }
}
